require 'spec_helper'
require 'fxos_rails/manifest'

describe FxosRails::Manifest do
  it "accepts a hash" do
    m = FxosRails::Manifest.new({
      name: "My app",
      description: "App to do cool stuff, anywhere, anytime",
      icons: {},
    })

    j = m.as_json
    expect(j['name']).to eql("My app")
    expect(j['icons']).to eql({})
  end
end
