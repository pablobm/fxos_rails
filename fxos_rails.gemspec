$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "fxos_rails/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "fxos_rails"
  s.version     = FxosRails::VERSION
  s.date        = Time.now.utc.strftime("%Y-%m-%d")
  s.authors     = ["Pablo Brasero"]
  s.email       = ["pablo@pablobm.com"]
  s.homepage    = "https://bitbucket.org/pablobm/fxos_rails"
  s.summary     = "FirefoxOS utilities for Ruby on Rails"
  s.license     = 'MIT'

  s.files = `git ls-files`.split("\n")

  s.add_dependency "rails", "~> 4.1.0"
  s.add_dependency "nokogiri", "~> 1"
  s.add_development_dependency "rspec"
end
