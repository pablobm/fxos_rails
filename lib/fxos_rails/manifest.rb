require 'active_support/core_ext/hash/keys'

module FxosRails
  class Manifest

    def initialize(hash)
      @hash = hash
    end

    def as_json(*)
      @hash.stringify_keys
    end

  end
end
