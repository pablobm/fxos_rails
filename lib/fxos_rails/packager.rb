require 'nokogiri'

module FxosRails
  class Packager

    class Error < StandardError; end
    class DownloadError < Error; end
    class BuildError < Error; end

    def clean!
      system <<-SHELL
        rm -rf #{tmp_path}/#{app_host} &&
        rm -rf public/assets
      SHELL
    end

    def package!
      check_environment_exists!
      precompile
      with_local_build_server do
        download
      end
      check_download_success!
      unify_js
      add_manifest
      put_files_in_package
    rescue Error
      puts $!.message
    end


    private

    def build_env
      'build'
    end

    def ip
      '127.0.0.1'
    end

    def port
      3001
    end

    def with_local_build_server
      app_url  = "http://#{app_host}/#{launch_path}"
      run %{DISABLE_SPRING=1 ./bin/rails server -e #{build_env} -p #{port} -b #{ip} -P #{pid_path} -d}
      yield(app_url)
    ensure
      run %{kill -TERM `cat #{pid_path}`}
    end

    def pid_path
      tmp_path.join('pids', 'build.pid')
    end

    def app_host
      "#{ip}:#{port}"
    end

    def add_manifest
      man = FxosRails.manifest.to_json
      File.write(local_app_path + 'manifest.webapp', man)
    end

    def put_files_in_package
      puts "Packaging..."
      package_path = local_app_path.join(package_filename)
      run %{cd #{local_app_path} && zip #{package_path} -r .}
      puts "Done. The package is now at #{package_path}"
    end

    def precompile
      run %{RAILS_ENV=#{build_env} ./bin/rake assets:precompile}
    end

    def check_download_success!
      return unless download_errors.present?
      bullet = '  * '
      errors_list = bullet + download_errors.join("\n" + bullet)
      msg = <<-EOM
ERROR: failed to grab some application files. Common causes include:
  * Your app cannot be accessed at #{launch_path}
  * Your app doesn't work locally when using the `#{build_env}` environment. For example, it could be missing a `#{build_env}` entry on `config/database.yml`
  * Your app doesn't serve static files. Ensure the setting `config.serve_static_assets = true` on `config/environments/#{build_env}.rb`
  * Some third-party assets are not being precompiled. This can happen with assets from gems. Ensure they are listed in the setting `config.assets.precompile` on `config/application.rb`

Specifically, the following URLs returned errors:
#{errors_list}
      EOM
      raise DownloadError, msg
    end

    def environment_config_path
      Rails.root.join('config', 'environments', "#{build_env}.rb")
    end

    def check_environment_exists!
      return if Rails.root.join(environment_config_path).exist?
      msg = <<-EOM
The build process requires a `#{build_env}` environment. Please create it at #{environment_config_path}. You can simply follow these steps:
  1. Copy the production environment from config/environments/production.rb
  2. Edit the file to ensure the setting `config.serve_static_assets = true`
  3. Done!
EOM
      raise BuildError, msg
    end

    def download
      run %{cd #{tmp_path} && wget -nv -o #{wget_log_path} -r #{launch_url} -k}
      remove_query_strings
      add_icons
    end

    def download_errors
      @download_errors ||= begin
        last_url = nil
        url_re = %r{\b(https?://[^ ]+):}
        timestamp_re = %r{\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d}
        error_re = %r{#{timestamp_re} ERROR}
        errors = []
        wget_log_path.open.each_line do |line|
          if url_re =~ line
            last_url = $~[1]
          elsif error_re =~ line
            errors << last_url
          end
        end
        errors.reject{|url| url =~ %r{/robots.txt$} }
      end
    end

    def download_successful?
      download_errors.blank?
    end

    def remove_query_strings
      Dir[local_app_path + '**' + '*'].each do |path|
        if path['?']
          FileUtils.mv(path, path.gsub(/\?.*$/, ''))
        end
      end
    end

    def add_icons
      icons_path = Rails.root.join(*%w{app assets images icons})
      if icons_path.exist?
        FileUtils.cp_r(icons_path, local_app_path)
      end
    end

    def unify_js
      chunks = []
      doc = nil
      File.open(local_launch_path, 'r+') do |f|
        doc = Nokogiri::HTML(f)
        doc.css('script').each do |node|
          next if not unifiable_js?(node)
          if src = node['src']
            src.gsub!(%r{^/}, '')
            full_path = local_app_path.join(src)
            chunks << File.read(full_path)
            FileUtils.rm(full_path)
          else
            chunks << node.content
          end
          node.remove
        end
        new_script_relpath = 'assets/app.js'
        new_script_path = File.join(local_app_path, new_script_relpath)
        File.write(new_script_path, chunks.join("\n"))
        new_script_node = Nokogiri::XML::Node.new('script', doc)
        new_script_node['src'] = new_script_relpath
        doc.css('body').last.children.after(new_script_node)
      end
      File.write(local_launch_path, doc.to_html)
    end

    def unifiable_js?(node)
      type = node['type']
      type.blank? || type != 'text/javascript' || node['data-unify'] == 'ignore'
    end

    def package_filename
      basename = I18n.t('app.name').parameterize
      "#{basename}.fxos.zip"
    end

    def tmp_path
      Rails.root.join('tmp')
    end

    def launch_url
      'http://' + app_host + '/' + launch_path
    end

    def launch_path
      'index.html'
    end

    def local_app_path
      tmp_path + app_host
    end

    def local_launch_path
      local_app_path + launch_path
    end

    def run(command)
      puts "*** #{command}"
      system(command)
    end

    def wget_log_path
      tmp_path.join('wget.log')
    end

  end
end
