require 'rails'

module FxosRails
  class Railtie < Rails::Railtie
    railtie_name :fxos_rails

    rake_tasks do
      load 'tasks/fxos_tasks.rake'
    end
  end
end
