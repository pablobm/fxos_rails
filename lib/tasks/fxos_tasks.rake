require 'fxos_rails/packager'

namespace :fxos do
  desc "Delete files created by the build process"
  task :clean do
    FxosRails::Packager.new.clean!
  end

  desc "Build your app's package"
  task build: %w{clean environment} do
    FxosRails::Packager.new.package!
  end
end



