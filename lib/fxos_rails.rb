module FxosRails
  require 'fxos_rails/railtie' if defined?(Rails)
  require 'fxos_rails/manifest'

  cattr_accessor(:manifest) { {} }
end

